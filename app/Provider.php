<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model {

    /**
     * Get provider's id (concatenation of first and last names)
     *
     * @return string
     */
    public function getProviderId()
    {
        return $this->first_name.$this->last_name;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'clinic_id',
    ];

}
