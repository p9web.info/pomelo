<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Availability;
use App\Patient;
use App\Provider;

class AppointmentsController extends Controller
{
    // 15 minutes converted into timestamp integer
    private $time_slot_length = 60 * 60 * 15;

    // Useful function that rounds given timestamp to the closest quarter of hour
    private function round_timestamp($timestamp) {
        return round($timestamp / (15 * 60)) * (15 * 60);
    }

    /**
     * Returns a list of appointments for a specific provider in a specific timeframe
     *
     * @var $provider_id string
     * @var $from integer
     * @var $to integer
     *
     * @return array
     */
    public function find($provider_id, $from, $to)
    {
        $now = time();
        if ($from < $now)
        {
            $from = $now;
        }

        if ($to < $now)
        {
            // FIXME: Extremely poor error handling here. Sorry...
            abort(400);
            return [];
        }

        request()->validate([
            'provider_id' => ['string'],
            'from' => ['integer'],
            'to' => ['integer', 'gt:from'],
        ]);

        // An appointment is an availability with no patient_id
        $appointments = Availability::where('provider_id', $provider_id)
            ->whereNotNull('patient_id')
            ->where('start_timestamp', '>=', $from)
            ->where('start_timestamp', '<', ($to + $this->time_slot_length))
            ->get();

        return ['appointments' => $appointments];
    }


    /**
     * Returns a provider's appointment at a specific timestamp
     *
     * @var $provider_id string
     * @var $start_timestamp integer
     *
     * @return array
     */
    public function get($provider_id, $start_timestamp)
    {
        $start_timestamp = $this->round_timestamp($start_timestamp);

        request()->validate([
            'provider_id' => ['string'],
            'start_timestamp' => ['integer'],
        ]);

        // An appointment is nothing but an availability with no patient_id
        $appointment = Availability::where('provider_id', $provider_id)
            ->whereNotNull('patient_id')
            ->where('start_timestamp', $start_timestamp)
            ->first();

        return ['appointment' => $appointment];
    }


    /**
     * Creates a new appointment for a specific provider in at specific timestamp with a specific patient
     *
     * @var $provider_id string
     * @var $patient_id string
     * @var $start_timestamp integer
     *
     * @return array
     */
    public function create(Request $request)
    {
        $start_timestamp = $this->round_timestamp($request->start_timestamp);

        request()->validate([
            'provider_id' => ['string'],
            'patient_id' => ['string'],
            'start_timestamp' => ['integer'],
        ]);

        $availability = Availability::where('provider_id', $request->provider_id)
            ->where('start_timestamp', $start_timestamp)
            ->whereNull('patient_id')
            ->first();

        $provider = Provider::orWhere(Provider::raw("first_name || last_name"), $request->provider_id)
            ->first();

        $patient = Patient::orWhere(Patient::raw("first_name || last_name"), $request->patient_id)
            ->first();

        if (!$availability || !$patient || !$provider)
        {
            // FIXME: Extremely poor error handling here. Sorry...
            abort(404);
            return [];
        }

        $patient_double_booking = Availability::where('patient_id', $request->patient_id)
            ->where('start_timestamp', $start_timestamp)
            ->first();

        if ($patient_double_booking)
        {
            // FIXME: Extremely poor error handling here. Sorry...
            abort(404, 'Patient already booked');
            return [];
        }

        Availability::where('provider_id', $request->provider_id)
            ->where('start_timestamp', $start_timestamp)
            ->update([
                'patient_id' => $request->patient_id,
            ]);

        return ['appointment' => Availability::where('provider_id', $request->provider_id)
            ->where('start_timestamp', $start_timestamp)
            ->get()];
    }
}
