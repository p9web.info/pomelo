<?php

namespace App\Http\Controllers;

use App\Provider;
use App\Clinic;
use Illuminate\Http\Request;

class ProvidersController extends Controller
{
    /**
     * Function that returns all patients
     *
     * @return array
     */
    public function find()
    {
        return ['providers' => Provider::all()];
    }


    /**
     * Function that returns a provider that matches an id
     *
     * @return array
     */
    public function get($id)
    {
        request()->validate(['id' => ['string']]);

        $provider = Provider::orWhere(Provider::raw("first_name || last_name"), $id)
            ->first();

        if (!$provider)
        {
            // FIXME: Extremely poor error handling here. Sorry...
            abort(404);
            return [];
        }

        return ['provider' => $provider];
    }


    /**
     * Function that creates a new provider
     *
     * @return array
     */
    public function create(Request $request)
    {
        $request->validate([
            'clinic_id' => ['required', 'string'],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
        ]);

        $clinic = Clinic::where('clinic_id', $request->clinic_id)
            ->first();

        if (!$clinic) {
            // FIXME: Extremely poor error handling here. Sorry...
            abort(404);
            return [];
        }

        $payload = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'clinic_id' => $request->clinic_id,
        ];

        $provider = Provider::create($payload);

        return ['provider' => $provider];
    }
}
