<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clinic;

class ClinicsController extends Controller
{
    /**
     * Function that returns all clinics
     *
     * @return array
     */
    public function find()
    {
        $clinics = Clinic::all();
        return ['clinics' => $clinics];
    }


    /**
     * Function that returns a patient that matches an id
     *
     * @var $id string
     *
     * @return array
     */
    public function get($id)
    {
        request()->validate([
            'clinic_id' => ['string'],
        ]);

        $clinic = Clinic::where('clinic_id', $id)
            ->first();

        if (!$clinic) {
            // FIXME: Extremely poor error handling here. Sorry...
            abort(404);
            return [];
        }

        return ['clinic' => $clinic];
    }


    /**
     * Function that creates a new patient
     *
     * @var $request Request
     * @return array
     */
    public function create(Request $request)
    {
        $request->validate([
            'clinic_id' => ['required', 'string'],
        ]);

        $clinic = Clinic::create([
            'clinic_id' => $request->clinic_id,
        ]);

        return ['clinic' => $clinic];
    }
}
