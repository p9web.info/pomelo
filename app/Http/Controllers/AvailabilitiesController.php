<?php

namespace App\Http\Controllers;

use App\Availability;
use Illuminate\Http\Request;

class AvailabilitiesController extends Controller
{
    // 15 minutes converted into timestamp integer
    private $time_slot_length = 60 * 60 * 15;

    // Useful function that rounds given timestamp to the closest quarter of hour
    private function round_timestamp($timestamp) {
        return round($timestamp / (15 * 60)) * (15 * 60);
    }

    /**
     * Returns a list of availabilities for a specific provider in a specific timeframe
     *
     * @var $provider_id string
     * @var $from integer
     * @var $to integer
     *
     * @return array
     */
    public function find($provider_id, $from, $to)
    {
        $now = time();
        if ($from < $now)
        {
            $from = $now;
        }

        if ($to < $now)
        {
            abort(400);
            return [];
        }

        request()->validate([
            'provider_id' => ['string'],
            'from' => ['integer'],
            'to' => ['integer', 'gt:from'],
        ]);

        $availabilities = Availability::where('provider_id', $provider_id)
            ->whereNull('patient_id')
            ->where('start_timestamp', '>=', $from)
            ->where('start_timestamp', '<', ($to + $this->time_slot_length))
            ->get();

        return ['availabilities' => $availabilities];
    }


    /**
     * Returns a provider's availability at a specific timestamp
     *
     * @var $provider_id string
     * @var $start_timestamp integer
     *
     * @return array
     */
    public function get($provider_id, $start_timestamp)
    {
        $start_timestamp = $this->round_timestamp($start_timestamp);

        request()->validate([
            'provider_id' => ['string'],
            'start_timestamp' => ['integer'],
        ]);

        // An appointment is nothing but an availability with no patient_id
        $availability = Availability::where('provider_id', $provider_id)
            ->whereNull('patient_id')
            ->where('start_timestamp', $start_timestamp)
            ->first();

        return ['availability' => $availability];
    }


    /**
     * Creates a new availability for a specific provider in at specific moment
     *
     * @var $request Request
     *
     * @return array
     */
    public function create(Request $request)
    {
        $start_timestamp = $this->round_timestamp($request->start_timestamp);
        // $start_timestamp = $this->round_timestamp(time());

        $now = time();
        if ($start_timestamp < $now)
        {
            // TODO: Extremely poor error handling here. Sorry...
            abort(404);
            return [];
        }

        request()->validate([
            'provider_id' => ['string'],
            'start_timestamp' => ['integer'],
        ]);

        $availability = Availability::create([
            'provider_id' => $request->provider_id,
            'patient_id' => null,
            'start_timestamp' => $start_timestamp,
            'end_timestamp' => ($start_timestamp + $this->time_slot_length),
        ]);

        return ['availability' => $availability];
    }
}
