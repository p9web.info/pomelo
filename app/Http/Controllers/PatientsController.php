<?php

namespace App\Http\Controllers;

use App\Patient;
use App\Clinic;
use Illuminate\Http\Request;

class PatientsController extends Controller
{
    /**
     * Function that returns all patients
     *
     * @return array
     */
    public function find()
    {
        $patients = Patient::all();
        return ['patients' => $patients];
    }


    /**
     * Function that returns a patient that matches an id
     *
     * @var $id string
     *
     * @return array
     */
    public function get($id)
    {
        request()->validate(['id' => ['string']]);

        $patient = Patient::orWhere(Patient::raw("first_name || last_name"), $id)
            ->first();

        if (!$patient)
        {
            // FIXME: Extremely poor error handling here. Sorry...
            abort(404);
            return [];
        }

        return ['patient' => $patient];
    }


    /**
     * Function that creates a new patient
     *
     * @return array
     */
    public function create(Request $request)
    {
        $request->validate([
            'clinic_id' => ['required', 'string'],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
        ]);

        $clinic = Clinic::where('clinic_id', $request->clinic_id)
            ->first();

        if (!$clinic) {
            // FIXME: Extremely poor error handling here. Sorry...
            abort(404);
            return [];
        }

        $patient = Patient::create([
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'clinic_id' => request('clinic_id'),
        ]);

        return ['patient' => $patient];
    }
}
