<?php

use Illuminate\Support\Facades\Route;


// TODO: Create routes for PUT and DELETE methods
Route::get('/clinics', 'ClinicsController@find');
Route::get('/clinic/{id}', 'ClinicsController@get');
Route::post('/clinic/', 'ClinicsController@create');


// TODO: Create routes for PUT and DELETE methods
Route::get('providers', 'ProvidersController@find');
Route::get('provider/{id}', 'ProvidersController@get');
Route::post('provider', 'ProvidersController@create');


// TODO: Create routes for PUT and DELETE methods
Route::get('patients', 'PatientsController@find');
Route::get('patient/{id}', 'PatientsController@get');
Route::post('patient', 'PatientsController@create');


// TODO: Create routes for PUT and DELETE methods
Route::get('availabilities/{provider_id}/{from}/{to}', 'AvailabilitiesController@find');
Route::get('availability/{provider_id}/{start_timestamp}', 'AvailabilitiesController@get');
Route::post('availability', 'AvailabilitiesController@create');


// TODO: Create routes for PUT and DELETE methods
Route::get('appointments/{provider_id}/{from}/{to}', 'AppointmentsController@find');
Route::get('appointment/{provider_id}/{start_timestamp}', 'AppointmentsController@get');
Route::post('appointment', 'AppointmentsController@create');
