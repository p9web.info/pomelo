<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{

    // Useful function that rounds given timestamp to the closest quarter of hour
    private function round_timestamp($timestamp) {
        return round($timestamp / (15 * 60)) * (15 * 60);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time_slot_length = 60 * 60 * 15;
        $now = $this->round_timestamp(time());

        for($i = 0; $i < 50; $i++)
        {
            $rnd_clinic_id = Str::random(10);
            $rnd_provider_fname = Str::random(10);
            $rnd_provider_lname = Str::random(10);
            $rnd_patient_fname = Str::random(10);
            $rnd_patient_lname = Str::random(10);
            $rnd_timestamp = $now + ($time_slot_length * $i);

            DB::table('clinics')->insert([
                'clinic_id' => $rnd_clinic_id,
            ]);

            DB::table('patients')->insert([
                'first_name' => $rnd_patient_fname,
                'last_name' => $rnd_patient_lname,
                'clinic_id' => $rnd_clinic_id,
            ]);

            DB::table('providers')->insert([
                'first_name' => $rnd_provider_fname,
                'last_name' => $rnd_provider_lname,
                'clinic_id' => $rnd_clinic_id,
            ]);

            DB::table('availabilities')->insert([
                'provider_id' => $rnd_provider_fname.$rnd_provider_lname,
                'patient_id' => null,
                'start_timestamp' => $rnd_timestamp,
                'end_timestamp' => $time_slot_length + $rnd_timestamp,
            ]);
        }
    }
}
