# Pomelo - Laravel

## Requested features:
1. A patient can search for the different providers of the clinic
1. A patient can look for the availabilities of a specific provider within a defined time interval (for instance, the availabilities of Dr. A between May 8th, 2019 and May 12th, 2019)
1. A patient can book an appointment with a provider by selecting one of their availabilities

Implement a simplified REST API that will support the aforementioned features.

### Notes:
- Clinic
  1. Only one clinic is to be supported
  1. A clinic has only one attribute, its name, which also acts as its unique identifier (ID)
  1. A clinic can have one or several patients
  1. A clinic can have one or several providers
- Patient
  1. A patient has the following attributes: First name, Last name, The combination of the first and last names acts as an ID
- Provider
  1. A provider has the following attributes: First name, Last name, The combination of the first and last names acts as an ID
- Availability
  1. An availability is a time-slot during which a provider is able to treat a patient, if the latter has booked an appointment
  1. Each provider has one or several availabilities everyday
  1. An availability has the following attributes: Start date and time (timestamp), End date and time (timestamp), Availabilities are 15-minutes time-slots (8:00, 8:15, 8:30, etc.)
- Appointment
  1. An availability that's been chosen by a patient is converted into an appointment upon booking (one availability = one appointment)
  1. An appointment has the following attributes: Patient, Provider, Start date and time (timestamp), End date and time (timestamp)
  1. As appointments are booked on availabilities, they are also 15-minutes time-slots

---
## DB Structure:
- Clinics
  - name (primary key)
- Patients
  - first_name
  - last_name
  - clinic_id
  * Primary key: first_name.last_name
- Providers
  - first_name
  - last_name
  - clinic_id
  * Primary key: first_name.last_name
- Time_Slots
  - provider_id
  - start_datetime
  - end_datetime
  - clinic_id
  - booked_patient_id

### Notes:
- Prevent patient double-booking

## Routes:
- Providers
  - GET /providers/:clinic_id (returns providers by clinic)
- Availabilities
  - GET /availabilities/:provider_id/:from/:to (returns unbooked time slots by provider in a specific timeframe)
- Apointments
  - POST /appointment/:clinic_id/:provider_id/:patient_id/:start_datetime/:end_datetime (turns availalibity into appointment)

## TODO:
  [x] Figure out why classes can't be found (some articles mention differences between v.7 & v.8)
  [x] Move all web.php routes to api.php
  [x] Seed db
